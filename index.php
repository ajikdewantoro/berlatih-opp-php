<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Berlatih OOP PHP </title>
</head>
<body>
    <?php
        require_once("Animal.php");
        require("Frog.php");
        require("Ape.php");

        $sheep = new Animal("shaun");
        echo "Animal Name : $sheep->name <br>"; //"sahun"
        echo "Legs : $sheep->legs <br>";
        echo "Cold blooded : $sheep->cold_blooded <br><br>"; //false

        $kodok = new Frog("Buduk");
        echo "Animal Name : $kodok->name <br>";
        echo "Legs  : $kodok->legs1 <br>";
        echo "Jump : " . $kodok->jump(). '<br><br>'; //"hop-hp"

        $ape = new Ape("kera sakti");
        echo "Animal Name : $ape->name <br>";
        echo "Legs : $ape->legs <br>";
        echo "Yell : " . $ape->yell(). '<br>'; //"Auooo"



    ?>
    
</body>
</html>
