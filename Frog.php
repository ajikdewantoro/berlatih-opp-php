<?php

require_once('Animal.php');

class Frog extends Animal{
    public function __construct($name, $legs1 = 4){
        parent::__construct($name, $legs1);
    }

    public function jump(){
        return 'hop - hop';
    }

}

?>
