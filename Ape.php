<?php

require_once('Animal.php');

class Ape extends Animal{
    public function __construct($name, $legs1 = 2){
        parent::__construct($name, $legs1);
    }

    public function yell(){
        return 'Auooo';
    }

}

?>
